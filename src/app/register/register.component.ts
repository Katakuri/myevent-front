import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CitiesService } from '../services/cities.service';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Router } from '@angular/router';
import { element } from 'protractor';
import {MatSnackBar} from '@angular/material/snack-bar';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  cities = [];
  filteredOptions: Observable<string[]>;
  options = [];
  myControl = new FormControl();
  confPass : String = '';

  constructor(
    public http: HttpClient,
    public citieService: CitiesService,
    private router: Router,
    private _snackBar: MatSnackBar

  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      sexe : new FormControl('', [Validators.required, Validators.maxLength(30)]),
      firstName : new FormControl('', [Validators.required, Validators.maxLength(30)]),
      lastName : new FormControl('', [Validators.required, Validators.maxLength(30)]),
      age : new FormControl('', [Validators.required, Validators.maxLength(30)]),
      email : new FormControl('', [Validators.required, Validators.email, Validators.maxLength(30)]),
      city : this.myControl,
      password : new FormControl('', [Validators.required, Validators.maxLength(15)]),
    });

    this.citieService.getCities().then(() => this.citieService.cities.forEach((element) => {
      this.options.push(element);
    }));

    console.log(this.options);


    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 10000,
    });
  }

  confirmPass(){
    
    if(this.confPass.length === 0 || this.form.get('password').value.length === 0 ) {
      return false;
        } else if (this.confPass === this.form.get('password').value) {
             return true
        } else {
             return false
    }
  }


  submit(form) {
    this.http.post('http://localhost:4000/api/register', form.value).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('completed !');
        this.openSnackBar('Votre compte a bien été enregistré! Vous avez reçu un mail', "Fermer")
        this.router.navigate(['/login']);
      }
    );
  }
}
