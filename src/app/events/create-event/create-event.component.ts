import { Component, OnInit } from '@angular/core';

import { FormGroup, Validators, FormControl} from '@angular/forms';
import { Observable } from 'rxjs';
import { CitiesService } from 'src/app/services/cities.service';
import {map, startWith} from 'rxjs/operators';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthInterceptor} from '../../services/interceptor.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';



@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {

  form: FormGroup;
  myControl = new FormControl();
  cities = [];
  filteredOptions: Observable<string[]>;
  options = [];


  constructor(
    public citieService: CitiesService,
    public http: HttpClient,
    public authInter: AuthInterceptor,
    private router: Router,
    private _snackBar: MatSnackBar

  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      category : new FormControl('', [Validators.required]),
      description : new FormControl('', [Validators.required, Validators.maxLength(1000)]),
      nbAgeMin : new FormControl('', [Validators.required, Validators.min(10)]),
      nbParticipationMax : new FormControl('', [Validators.required, Validators.max(100)]),
      city_coord : this.myControl,
      title : new FormControl('', [Validators.required, Validators.max(50)]),
      dateStartEvent : new FormControl('', [Validators.required]),
      dateEndEvent : new FormControl('', [Validators.required])
    });

    this.citieService.getCities().then(() => this.citieService.cities.forEach((element) => {
      this.options.push(element);
    }));

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 10000,
    });
  }

  submit(form) {
    this.http.post('http://localhost:4000/event/addEvent', form.value).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      },
      () => {
        console.log('completed !');
        this.openSnackBar('Votre événement a bien été enregistré!', "Fermer")
        this.router.navigate(['/eventList']);
      }
    );
  }

}
