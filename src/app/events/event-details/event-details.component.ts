import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { Inject } from '@angular/core';
import { WeaherService } from '../../services/weaher.service';
import { EventService } from '../../services/event.service';
import * as moment from 'moment';


@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  weather: any;
  sunrise: any;
  sunset: any;
  clouds: any;
  tempMax: any;
  tempMin: any;
  tempActuel: any;
  placesRestantes: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private weatherService: WeaherService,
    private eventService: EventService

  ) { }

  ngOnInit() {
    console.log(this.data);
    this.getWeather(this.data.city_coord);
    this.placesRestantes = this.data.nbParticipationMax - this.data.idUsersParticipate.length;
  }

  getWeather(id: string) {
    this.weatherService.getWeathereCity(id).subscribe(
      (res) => {
        this.weather = res;
        this.sunrise = this.weatherService.unixToDate(this.weather.sys.sunrise);
        this.sunset = this.weatherService.unixToDate(this.weather.sys.sunset);
        this.clouds = this.weather.clouds.all;
        this.tempMax = this.weatherService.kelvinToCelsius(this.weather.main.temp_max);
        this.tempMin = this.weatherService.kelvinToCelsius(this.weather.main.temp_min);
        this.tempActuel = this.weatherService.kelvinToCelsius(this.weather.main.temp);
      },
      (err) => {
        console.log(err);
      },
      () => {
      });
  }

  diminue() {
    return this.placesRestantes - 1;
  }

  nbMaxAtteint() {
    if (this.placesRestantes === 0) {
      return true;
      }
    return false;
  }

  checkDate() {
    const toDay = moment();
    const eventDate = moment(this.data.dateEndEvent);
    if (toDay > eventDate ) {
      return true;
    }
    return false;
  }

  participateToevent(event) {
    this.eventService.participateToEvent(event).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => console.log(err),
      () => {
        this.diminue();
        console.log(this.placesRestantes);
      }
    );
  }


}
