import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CitiesService {

  cities: any[];

  constructor(
    public http: HttpClient
  ) { }

  url = 'http://localhost:4000/'

  getCities() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>(this.url+'save/allCities')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.cities = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getCityByName(name: string) {
    return this.http.get<any>(this.url+'save/cityByName/' + name);
  }
}
