import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  events = [];

  constructor(
    public http: HttpClient
  ) { }

  url = "http://localhost:4000/";

  getAllEvents() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>(this.url+'event/allEvents')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.events = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getEventsCompleted() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>(this.url+'event/getEventCompleted')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.events = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getEventsStill() {
    const promise = new Promise((resolve, reject) => {
      this.http.get<any>(this.url+'event/getEventsDontStart')
      .toPromise()
      .then(
        res => {
          console.log(res);
          this.events = res;
          resolve();
        },
        msg => {
          reject();
        },
        );
    });
    return promise;
  }

  getEventsByUser(){
    return this.http.get<any>(this.url+'api/getEventsByUser/');
  }

  participateToEvent(event) {
    return this.http.post(this.url+'event/participatEvent/' + event._id, event);
  }

  deleteUser(){
    return this.http.delete(this.url+'api/deleteUser/');
  }
}
