import { Component, OnInit } from '@angular/core';
import { EventService } from '../services/event.service';
import { ConfirmDeletedUserComponent } from '../about-user/confirm-deleted-user/confirm-deleted-user.component';
import {MatDialog} from '@angular/material/dialog';
import { Router } from '@angular/router';



@Component({
  selector: 'app-about-user',
  templateUrl: './about-user.component.html',
  styleUrls: ['./about-user.component.css']
})
export class AboutUserComponent implements OnInit {

  events = [];

  constructor(
    private eventService: EventService,
    public dialog: MatDialog,
    private router : Router

  ) { }

  ngOnInit() {
   this.eventService.getEventsByUser().subscribe(
      (res) => this.events = res,
      (err) => console.log(err));
  }

  deleteAccount(){
    this.eventService.deleteUser().subscribe( (res) => console.log(res),
      (err) => console.log(err));
  }

  logout(){
    localStorage.removeItem('id_token');
    this.router.navigate(['/login']);
  }

  openConfirmDelete(){
    const dialogRef = this.dialog.open(ConfirmDeletedUserComponent);

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.deleteAccount();
        this.logout();
      }
    });
  }



}
